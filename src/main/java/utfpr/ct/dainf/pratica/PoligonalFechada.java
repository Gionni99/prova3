/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author a1906453
 * @param <T>
 */
public class PoligonalFechada<T extends Ponto2D> extends Poligonal{

    @Override
    public double getComprimento(){
        double comp = 0;
        int i;
        for(i=0;i<getN()-1;i++){
            comp+=get(i).dist(get(i+1));
        }
        comp+=get(0).dist(get(getN()-1));
        return comp;
    }
}
