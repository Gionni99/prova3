package utfpr.ct.dainf.pratica;

import java.util.ArrayList;
import java.util.List;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <T> Tipo do ponto
 */
public class Poligonal<T extends Ponto2D> {
    private final List<T> vertices = new ArrayList<>();

    public int getN(){
        return vertices.size();
    }
    
    
    public T get(int i)throws getIException{
        if(i>=vertices.size())
            throw new getIException(i);
        return vertices.get(i);
    }
    
    
    public void set(int i,T p)throws SetIException{
        if(i>vertices.size())
            throw new SetIException(i);
        else if(i==vertices.size())
            vertices.add(p);
        else
            vertices.set(i, p);
    }
    
    public double getComprimento(){
        double comp = 0;
        for(int i=0;i<vertices.size()-1;i++){
            comp+=vertices.get(i).dist(vertices.get(i+1));
        }
        return comp;
    }
}
