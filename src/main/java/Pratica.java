
import utfpr.ct.dainf.pratica.PoligonalFechada;
import utfpr.ct.dainf.pratica.PontoXZ;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica {

    public static void main(String[] args) {
    PoligonalFechada<PontoXZ> poli;
        poli = new PoligonalFechada<>();
    PontoXZ a,b,c;
        a = new PontoXZ(-3.,2.);
        b = new PontoXZ(-3.,6.);
        c = new PontoXZ(0.,2.);
        
        poli.set(0, a);
        poli.set(1, b);
        poli.set(2, c);
        
        double comp=poli.getComprimento();
        
        System.out.println("O comprimento é de: " + comp);
    }
    
}
